package com.example.demo;

import java.util.List;

import lombok.Data;

@Data(staticConstructor = "of")
public class LineChartDataset {
	private final String label;
	private final List<Integer> data;
}
