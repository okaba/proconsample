package com.example.demo;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class XxxController {

	@GetMapping("/get")
	public ResponseEntity<LineChartDataset> get() {
		log.info("OK!");
		return ResponseEntity.ok().header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
				.body(LineChartDataset.of("data01", Arrays.asList(19, -41, 81, -12, 44, 1, 0)));
	}

	@GetMapping("/data")
	public ResponseEntity<List<Integer>> data() {
		log.info("OK!");
		return ResponseEntity.ok().header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
				.body(Arrays.asList(1, 13, 22, 65, 33, 93, 41));
	}

	@PostMapping("/table")
	public ResponseEntity<List<Record>> table(int i) {
		log.info("OK! i={}", i);
		return ResponseEntity.ok().header(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, "*")
				.body(Arrays.asList( //
				//						Record.of(1, "okaba", "xxx2", "male", 1, "abc", LocalDateTime.now(), 987), //
										Record.of(2, "abcde", "xxx3", "female", 2, "abc", LocalDateTime.now().minusDays(3), 987), //
										Record.of(3, "qwert", "xxx1", "male", 3, "abc", LocalDateTime.now().plusHours(3), 987) //
				));
	}

	@Data(staticConstructor = "of")
	public static class Record {
		private final long id;
		private final String name;
		private final String progress;
		private final String gender;
		private final int rating;
		private final String col;
		private final LocalDateTime dob;
		private final int driver;
	}
}
